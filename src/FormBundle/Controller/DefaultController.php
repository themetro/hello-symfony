<?php

namespace FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FormBundle:Default:index.html.twig');
    }

    public function createAction(Request $request, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('send@example.com')
        ->setTo('recipient@example.com')
        ->setBody(
            $this->renderView(
                'Emails/pqrd-registration.html.twig',
                [
                    "variable" => "valor"
                ]
            ),
            'text/html'
        );

        $mailer->send($message);

        return $this->renderString("test");
    }
}
