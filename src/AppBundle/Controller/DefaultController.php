<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    
    /**
     * @Route("/ayuda")
     */
    public function ayudaAction()
    {
        return new Response('Página de ayuda');
    }
    /**
     * @Route("/send")
     */
    public function sendAction()
    {
        $this_is = 'this is';
        $the_message = ' the message of the email';
        $mailer = $this->get('mailer');

        $message = \Swift_Message::newInstance()
            ->setSubject('The Subject for this Message')
            ->setFrom("hermelopezp@gmail.com")
            ->setTo('anderperez15@gmail.com')
            ->addPart($this->renderView('default/email.html.twig', ['this'=>$this_is, 'message'=>$the_message]))
        ;
        $mailer->send($message);
        return new Response('<html><body>The email has been sent successfully!</body></html>');
    }
}
